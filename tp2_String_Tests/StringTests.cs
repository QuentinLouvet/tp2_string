﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using tp2_String;

namespace tp2_String_Tests
{
    [TestClass]
    public class StringTests
    {
        [TestMethod]
        public void Uppercase()
        {
            Assert.AreEqual(1, Program.Uppercase('A'));
            Assert.AreEqual(0, Program.Uppercase('a'));
            Assert.AreEqual(0, Program.Uppercase('1'));
        }
        [TestMethod]
        public void Dot()
        {
            Assert.AreEqual(1, Program.Dot('.'));
            Assert.AreEqual(0, Program.Dot('1'));
            Assert.AreEqual(0, Program.Dot('A'));
        }
    }
}
