﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tp2_String
{
    public class Program
    {
        public static int Uppercase(char FirstChar)
        {
            if(FirstChar>='A' && FirstChar<='Z')
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public static int Dot(char LastChar)
        {
            if(LastChar=='.')
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        static void Main(string[] args)
        {
            string Sentence;
            do
            {
                Console.WriteLine("Saisissez une phrase");
                Sentence = Console.ReadLine();
            } while (Sentence.Length == 0);
            Console.WriteLine("Vous avez saisi : {0}", Sentence);
            if (Uppercase(Sentence[0]) == 1)
            {
                Console.WriteLine("La phrase saisie commence par une majuscule");
            }
            else
            {
                Console.WriteLine("La phrase saisie ne commence pas par une majuscule");
            }
            if (Dot(Sentence[Sentence.Length - 1]) == 1)
            {
                Console.WriteLine("La phrase saisie se termine par un point");
            }
            else
            {
                Console.WriteLine("La phrase saisie ne se termine pas par un point");
            }
            Console.ReadKey();
        }
    }
}
